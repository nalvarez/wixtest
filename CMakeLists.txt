cmake_minimum_required(VERSION 2.8)

project(foo C)
set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}")
# I don't actually know what version I need, but I'm testing with 3.7
find_package(Wix 3.7 REQUIRED)

add_executable(test WIN32 test.c version.rc)
set_target_properties(test PROPERTIES LINK_FLAGS "\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df'\"")

set(WIX_CANDLE_FLAGS -arch x64)
wix_compile(WIX_OBJS installer.wxs)
wix_link(installer.msi ${WIX_OBJS} DEPENDS test)
