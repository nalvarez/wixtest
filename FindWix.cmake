# - Find WiX executables
#=============================================================================
# Copyright 2013 Nicolás Alvarez
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# * Redistributions of source code must retain the above copyright
#   notice, this list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in the
#   documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

set(_WIX_VERSIONS 4.0 3.9 3.8 3.7 3.6 3.5 3.0)

foreach(_VERSION ${_WIX_VERSIONS})
    # CMake is a 32-bit process, so on 64-bit systems, its access to the
    # registry is normally redirected to Wow6432Node. However, CMake has
    # explicit logic to look in the 32-bit or 64-bit registry. The policy for
    # resolving registry paths is to get the same registry view as the
    # compiled app would see; in other words, access the 64-bit registry if
    # we're compiling for 64-bit (according to CMAKE_SIZEOF_VOID_P). However,
    # this doesn't work for WiX, as we don't care if we get a 32-bit WiX when
    # compiling 64-bit apps. This is why we need to manually look in both
    # registry keys.
    list(APPEND _REG_SEARCH_PATHS
        "[HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows Installer XML\\${_VERSION};InstallRoot]"
        "[HKEY_LOCAL_MACHINE\\SOFTWARE\\Wow6432Node\\Microsoft\\Windows Installer XML\\${_VERSION};InstallRoot]"
    )
endforeach()

find_path(
    WIX_PATH
    candle.exe
    PATHS ${_REG_SEARCH_PATHS}
)

execute_process(
    COMMAND "${WIX_PATH}/candle.exe" "-help"
    OUTPUT_VARIABLE _CANDLE_HELP_OUTPUT
)
string(REGEX REPLACE "^([^\n]*)\n.*" "\\1" _CANDLE_HELP_OUTPUT "${_CANDLE_HELP_OUTPUT}")
string(REGEX REPLACE "^(Microsoft \\(R\\) )?Windows Installer Xml Compiler version ([0-9]+(\\.[0-9]+)*)$" "\\2" WIX_VERSION "${_CANDLE_HELP_OUTPUT}")

find_program(WIX_CANDLE_PATH candle.exe PATHS ${WIX_PATH} NO_DEFAULT_PATH)
find_program(WIX_LIGHT_PATH  light.exe  PATHS ${WIX_PATH} NO_DEFAULT_PATH)
find_program(WIX_LIT_PATH    lit.exe    PATHS ${WIX_PATH} NO_DEFAULT_PATH)

# WIX_COMPILE(obj_files source1 source2...)
# Compiles the given .wxs sources, and sets the variable named by ${obj_files}
# to the list of generated .wixobj files, to be passed to WIX_LINK later.
function(WIX_COMPILE obj_files)
    foreach(source ${ARGN})
        get_filename_component(source ${source} ABSOLUTE)
        get_filename_component(basename ${source} NAME_WE)
        set(OUTPUT_WIXOBJ "${basename}.wixobj")

        add_custom_command(
            OUTPUT ${OUTPUT_WIXOBJ} ${basename}.wixpdb
            COMMAND "${WIX_CANDLE_PATH}"
            ARGS -nologo ${WIX_CANDLE_FLAGS} ${source}
            MAIN_DEPENDENCY ${source}
        )
        set(${obj_files} ${${obj_files}} ${OUTPUT_WIXOBJ} PARENT_SCOPE)
    endforeach()
endfunction()

function(WIX_LINK MSI_PATH)
    set(DEPS "")
    set(OBJECTS "")
    foreach(ARG ${ARGN})
        if ("${ARG}" STREQUAL DEPENDS)
            set(EXPECT_NEXT DEPENDS)
        elseif("${EXPECT_NEXT}" STREQUAL DEPENDS)
            list(APPEND DEPS ${ARG})
        else()
            list(APPEND OBJECTS ${ARG})
        endif()
    endforeach()

    add_custom_command(
        OUTPUT ${MSI_PATH}
        COMMAND "${WIX_LIGHT_PATH}"
        ARGS -nologo -out "${MSI_PATH}" ${OBJECTS}
        DEPENDS ${OBJECTS} ${DEPS}
    )
    add_custom_target(installer DEPENDS ${MSI_PATH})
endfunction()


include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Wix
    REQUIRED_VARS WIX_PATH WIX_CANDLE_PATH WIX_LIGHT_PATH WIX_LIT_PATH
    VERSION_VAR WIX_VERSION)

